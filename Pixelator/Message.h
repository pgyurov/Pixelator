#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <fstream>
using namespace std;
class Message
{
private:
	string _message, _line;
	int _linelength = 0;
	int _numberoflines = 0;

public:
	
	void openfile(string);
	void prepare(); //prepare to encrypt;
	int getLine_length();
	int getNumber_of_lines();
	string getMessage();
};


#endif MESSAGE_H