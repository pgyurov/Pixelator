#include "Message.h"
#include "Encrypt.h"
#include "EasyBMP.h"
#include <iostream>
#include <fstream>
using namespace std;

//useful: http://www.cplusplus.com/reference/cctype/isspace/
//name idea: catapple
int main()
{
	Message mymessage;
	mymessage.openfile("C:/Users/Petar/Google Drive/My Projects/Encryption/Pixelator/Pixelator/Message.txt");
	mymessage.prepare();

	Encrypt encryption;
	encryption.encrypt_message(mymessage, true, true);

	return 0;
}

/*
======== KNOWN BUGS =========
- Can't handle apostrophes, numbers
- Doesn't work if message contains no readable characters
*/

/*
======== IDEAS ========
- To easily implement the harder alphabet cipher
  just encrypt normaly, then load the image.
  Then read each pixel and if it's coloured, turn to white,
  if it's white, turn to random colour! This way there is no
  need to change every letter's definition, though it will be
  slightly slower obviously.
*/