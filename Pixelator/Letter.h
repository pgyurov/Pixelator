#ifndef LETTER_H
#define LETTER_H
#include <vector>
using namespace std;

class Letter
{
private:
	vector<int> _00, _10, _20;
	vector<int> _01, _11, _21;
	vector<int> _02, _12, _22;
	int _rn1, _rn2, _rn3, _rn4, _rn5, _rn6, _rn7, _rn8, _rn9; //rn = random number
	vector <int> _list_of_rns;

public:

	void assign(char, bool);
	int getRed(int, int);
	int getGreen(int, int);
	int getBlue(int, int);
};

#endif

