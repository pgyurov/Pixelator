#include "Message.h"
#include <iostream>
#include <cctype>
#include <algorithm>

using namespace std;

void Message::openfile(string dir)
{
	ifstream myfile(dir);

	if (myfile.is_open())
	{
		while (getline(myfile, _line))
		{
			_message += _line + '\n';

			if (_line.size() > _linelength)
				_linelength = _line.size();

			_numberoflines++;
		}
		myfile.close();
	}

	else cout << "Unable to open file!";
}

void Message::prepare()
{
	string::iterator end_pos = remove(_message.begin(), _message.end(), ' ');
	_message.erase(end_pos, _message.end());
	transform(_message.begin(), _message.end(), _message.begin(), ::tolower); //tolower may cause problems with something other than ASCII
	_message.erase(remove(_message.begin(), _message.end(), '.'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), ','), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), ':'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), ';'), _message.end());
	//_message.erase(remove(_message.begin(), _message.end(), "'"), _message.end()); //need a way around the const char error which arises in algorithm
	_message.erase(remove(_message.begin(), _message.end(), '"'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '!'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '?'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '>'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '<'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '�'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '$'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '%'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '^'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '&'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '*'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '('), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), ')'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '-'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '_'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '+'), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '='), _message.end());
	_message.erase(remove(_message.begin(), _message.end(), '\t'), _message.end());
}

int Message::getLine_length()
{
	return _linelength;
}

int Message::getNumber_of_lines()
{
	return _numberoflines;
}

string Message::getMessage()
{
	return _message;
}